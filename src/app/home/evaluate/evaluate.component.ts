import { Component, EventEmitter, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { FormioComponent } from '@formio/angular';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import {LocalizedDatePipe} from '@universis/common';

const COMPONENT_STATES = {
  Unknown: 'unknown',
  Submitting: 'submitting',
  Submitted: 'submitted',
  InvalidToken: 'invalidToken',
  EventNotFound: 'eventNotFound',
  EvaluationPeriodNotStarted: 'evaluationPeriodNotStarted',
  CourseClosedOrNotAvailable: 'courseClosedOrNotAvailable'
};

@Component({
  selector: 'app-evaluate',
  templateUrl: './evaluate.component.html',
  styleUrls: ['./evaluate.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EvaluateComponent implements OnInit, OnDestroy {


  public evaluationEvent: any;
  public evaluationForm: any;
  public state = COMPONENT_STATES.Submitting;
  public submission: any = {
    data: {}
  };
  @ViewChild('confirmTemplate') confirmTemplate: TemplateRef<any>;
  @ViewChild('form') form: FormioComponent;
  queryParamsSubscription: Subscription;
  public success: EventEmitter<any> = new EventEmitter();
  public refresh: EventEmitter<any> = new EventEmitter();
  public start: string;
  public end: string;

  constructor(private context: AngularDataContext,
              private modalService: BsModalService,
              private activatedRoute: ActivatedRoute,
              public translateService: TranslateService) { }

  ngOnDestroy(): void {
    if (this.queryParamsSubscription) {
      this.queryParamsSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.queryParamsSubscription = this.activatedRoute.queryParams.subscribe((queryParams) => {
      // set component state
      this.state = COMPONENT_STATES.Submitting;
      // check for access token
      if (queryParams.access_token) {
        // get current evaluation
        sessionStorage.setItem('access_token', queryParams.access_token);
      }
      // get access token
      const accessToken = sessionStorage.getItem('access_token');
      if (accessToken) {
        this.context.getService().setHeader('Evaluation-Key', accessToken);
      }
      // get evaluation event
      this.context.model('EvaluationEvents/Current').asQueryable().getItem().then((evaluationEvent) => {
        // get form
        return this.context.model('EvaluationEvents/Current/Form').asQueryable().getItem().then((evaluationForm) => {
          const form = evaluationForm;
          const submit = form.components.find((item) => {
            return item.action === 'submit';
          });
          if (!submit)
          {
            // add submit button
            const submitComponent = {
              type: 'button',
              label: this.translateService.instant('Submit'),
              key: 'submit',
              disableOnInvalid: false,
              input: true,
              tableView: false
            };
            form.components.push(submitComponent);
          }
          if (submit && submit.validate) {
            submit.validate.required = false;
          }
          Object.keys(evaluationEvent).forEach(key => {
            const component = this.findByKey(form.components, key);
            if (component) {
              if (key === 'dateCreated') {
                component.type = 'datetime';
                component.defaultValue = new Date();
              } else {
                component.defaultValue = evaluationEvent[key];
              }
            }
          });
          this.evaluationForm = form;
          this.evaluationEvent = evaluationEvent;
          if (evaluationEvent) {
            this.start = new LocalizedDatePipe(this.translateService).transform(evaluationEvent.startDate, 'shortDate');
            this.end = new LocalizedDatePipe(this.translateService).transform(evaluationEvent.endDate, 'shortDate');
          }
          this.form.submitExecute = (submission: any, saved?: boolean) => {
            this.context.model('EvaluationEvents/Current/Evaluate').save(submission.data).then(() => {
              this.state = COMPONENT_STATES.Submitted;
              saved = true;
            }).catch((err) => {
              saved = false;
              console.log(err);
            });
          };
        });
      }).catch((err) => {
        if (err && err.status === 498) {
          this.state = COMPONENT_STATES.InvalidToken;
        }
        if (err && err.status === 404) {
          this.state = COMPONENT_STATES.EventNotFound;
        }
        if (err && err.error.code === 'E_NOT_STARTED') {
          this.state = COMPONENT_STATES.EvaluationPeriodNotStarted;
        }
        if (err && err.error.code === 'E_CLOSED_EVENT') {
          this.state = COMPONENT_STATES.CourseClosedOrNotAvailable;
        }
        console.error(err);
      });
    });
  }
  findByKey(array, key) {
    for (let index = 0; index < array.length; index++) {
      const element = array[index];
      if (element.key === key) {
        return element;
      } else {
        if (element.components) {
          const found = this.findByKey(element.components, key);
          if (found) {
            return found;
          }
        }
      }
    }
  }
}

