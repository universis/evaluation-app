import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ResponseError } from '@themost/client';
import {ConfigurationService} from '@universis/common';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  public applicationLogo: string;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private context: AngularDataContext,
              private translateService: TranslateService,
              private _configurationService: ConfigurationService) { }

  public lastError: Error;
  public loading = false;
  ngOnInit(): void {
    const appSettings: { image?: string; title?: string; logo?: string } = this._configurationService.settings.app;
    this.applicationLogo = appSettings && appSettings.logo;
  }
  onSubmit(form: NgForm): void {
    this.loading = true;
    const formData: { evaluationKey?: string } = form.value;
    this.context.getService().setHeader('Evaluation-Key', formData.evaluationKey);
    this.context.model('EvaluationEvents/Current').asQueryable().getItem().then(() => {
      this.loading = false;
      this.router.navigate([
        '..',
        'evaluate'
      ], {
        relativeTo: this.activatedRoute,
        queryParams: {
          access_token: formData.evaluationKey
        }
      });
    }).catch((err) => {
      this.loading = false;
      if (err && err.error) {
       if (err.error.code === 'E_NOT_STARTED') {
        this.lastError = new ResponseError(this.translateService.instant('EvaluationPeriodNotStarted'), 500);
      } else if (err.error.code === 'E_CLOSED_EVENT') {
         this.lastError = new ResponseError(this.translateService.instant('CourseClosedOrNotAvailable'), 500);
      } else if (err.error.statusCode && err.error.statusCode === 498) {
          this.lastError = new ResponseError(this.translateService.instant('InvalidEvaluationKey'), 498);
      } else {
          this.lastError = err.error;
      }
      } else {
        this.lastError = new Error('An error occurred while validating evaluation key. Please try again.');
      }
    });
  }

  clearError(): void {
    this.lastError = null;
  }

}
